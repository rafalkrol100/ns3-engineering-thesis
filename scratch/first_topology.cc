#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"
#include "ns3/lte-module.h"
#include "ns3/config-store.h"
#include "ns3/radio-bearer-stats-calculator.h"

#include <iomanip>
#include <string>

using namespace ns3;

int main (int argc, char *argv[])
{
    double enbDist = 100.0;
    double radius = 50.0;
    uint32_t numUes = 1;
    double simTime = 1.0;
    double foo = 21.37;

    CommandLine cmd;
    cmd.AddValue ("foo", "Total duration of the simulation (in seconds)", foo);
    cmd.Parse(argc, argv);

    ConfigStore inputConfig;
    inputConfig.ConfigureDefaults ();
    
    // parse again so you can override default values from the command line
    cmd.Parse (argc, argv);
    
    // determine the string tag that identifies this simulation run
    // this tag is then appended to all filenames

    UintegerValue runValue;
    GlobalValue::GetValueByName ("RngRun", runValue);

    std::ostringstream tag;
    tag  << "_enbDist" << std::setw (3) << std::setfill ('0') << std::fixed << std::setprecision (0) << enbDist
        << "_radius"  << std::setw (3) << std::setfill ('0') << std::fixed << std::setprecision (0) << radius
        << "_numUes"  << std::setw (3) << std::setfill ('0')  << numUes
        << "_rngRun"  << std::setw (3) << std::setfill ('0')  << runValue.Get () ;

    Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();
  
    lteHelper->SetAttribute ("PathlossModel", StringValue ("ns3::FriisSpectrumPropagationLossModel"));

    // Create Nodes: eNodeB and UE
    NodeContainer enbNodes;
    enbNodes.Create (4);
    NodeContainer ueNodes1, ueNodes2, ueNodes3, ueNodes4;
    ueNodes1.Create (numUes);
    ueNodes2.Create (numUes);
    ueNodes3.Create (numUes);
    ueNodes4.Create (numUes);

    // Position of eNBs
    Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
    positionAlloc->Add (Vector (0.0, 0.0, 0.0));
    positionAlloc->Add (Vector (enbDist, 0.0, 0.0));
    positionAlloc->Add (Vector (enbDist, enbDist, 0.0));
    positionAlloc->Add (Vector (0.0, enbDist, 0.0));
    MobilityHelper enbMobility;
    enbMobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
    enbMobility.SetPositionAllocator (positionAlloc);
    enbMobility.Install (enbNodes);

    // Position of UEs attached to eNB 1
    MobilityHelper ue1mobility;
    ue1mobility.SetPositionAllocator ("ns3::UniformDiscPositionAllocator",
                                        "X", DoubleValue (0.0),
                                        "Y", DoubleValue (0.0),
                                        "rho", DoubleValue (radius));
    ue1mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
    ue1mobility.Install (ueNodes1);

    // Position of UEs attached to eNB 2
    MobilityHelper ue2mobility;
    ue2mobility.SetPositionAllocator ("ns3::UniformDiscPositionAllocator",
                                        "X", DoubleValue (enbDist),
                                        "Y", DoubleValue (0.0),
                                        "rho", DoubleValue (radius));
    ue2mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
    ue2mobility.Install (ueNodes2);

    // Position of UEs attached to eNB 3
    MobilityHelper ue3mobility;
    ue3mobility.SetPositionAllocator ("ns3::UniformDiscPositionAllocator",
                                        "X", DoubleValue (enbDist),
                                        "Y", DoubleValue (0.0),
                                        "rho", DoubleValue (radius));
    ue3mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
    ue3mobility.Install (ueNodes3);

    // Position of UEs attached to eNB 2
    MobilityHelper ue4mobility;
    ue4mobility.SetPositionAllocator ("ns3::UniformDiscPositionAllocator",
                                        "X", DoubleValue (enbDist),
                                        "Y", DoubleValue (0.0),
                                        "rho", DoubleValue (radius));
    ue4mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
    ue4mobility.Install (ueNodes4);

    // Create Devices and install them in the Nodes (eNB and UE)
    NetDeviceContainer enbDevs;
    NetDeviceContainer ueDevs1;
    NetDeviceContainer ueDevs2;
    NetDeviceContainer ueDevs3;
    NetDeviceContainer ueDevs4;
    enbDevs = lteHelper->InstallEnbDevice (enbNodes);
    ueDevs1 = lteHelper->InstallUeDevice (ueNodes1);
    ueDevs2 = lteHelper->InstallUeDevice (ueNodes2);
    ueDevs3 = lteHelper->InstallUeDevice (ueNodes3);
    ueDevs4 = lteHelper->InstallUeDevice (ueNodes4);

    // Attach UEs to a eNB
    lteHelper->Attach (ueDevs1, enbDevs.Get (0));
    lteHelper->Attach (ueDevs2, enbDevs.Get (1));
    lteHelper->Attach (ueDevs3, enbDevs.Get (2));
    lteHelper->Attach (ueDevs4, enbDevs.Get (3));

    // Activate a data radio bearer each UE
    enum EpsBearer::Qci q = EpsBearer::GBR_CONV_VOICE;
    EpsBearer bearer (q);
    lteHelper->ActivateDataRadioBearer (ueDevs1, bearer);
    lteHelper->ActivateDataRadioBearer (ueDevs2, bearer);
    lteHelper->ActivateDataRadioBearer (ueDevs3, bearer);
    lteHelper->ActivateDataRadioBearer (ueDevs4, bearer);

    Simulator::Stop (Seconds (simTime));

    // Insert RLC Performance Calculator
    std::string dlOutFname = "DlRlcStats";
    dlOutFname.append (tag.str ());
    std::string ulOutFname = "UlRlcStats";
    ulOutFname.append (tag.str ());

    lteHelper->EnableMacTraces ();
    lteHelper->EnableRlcTraces ();

    Simulator::Run ();
    Simulator::Destroy ();
    return 0;
}

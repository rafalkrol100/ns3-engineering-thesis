#include <ns3/core-module.h>
#include <ns3/network-module.h>
#include <ns3/mobility-module.h>
#include <ns3/lte-module.h>
#include <ns3/config-store.h>
#include <ns3/radio-bearer-stats-calculator.h>
#include <ns3/internet-stack-helper.h>
#include <ns3/point-to-point-helper.h>
#include <ns3/ipv4-static-routing.h>
#include <ns3/ipv4-static-routing-helper.h>
#include <ns3/packet-sink-helper.h>
#include <ns3/udp-client-server-helper.h>
#include <ns3/epc-helper.h>
//#include "read_from_txt.hpp"
#include "ns3/point-to-point-module.h"
#include "ns3/internet-module.h"
#include "ns3/applications-module.h"
#include "ns3/config-store-module.h"
#include "ns3/data-collector.h"
#include "ns3/flow-monitor-helper.h"
#include <iomanip>
#include <string>
#include <vector>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>
#include <ctime>

using namespace ns3;

const int RRH = 4;
const int vehicle = 10;
const int dim = 2;

long double string_to_double(std::string text){
    long double number = 0;
    int it = 0;
    int cyfra = 0;
    long double temp = 0;
    long double decimalcounter = 1;

    for(int i = 0; i < text.size(); i++){
        if(text[i] == '.'){
            break;
        }
        cyfra = int(text[i]) - 48;
        number *= 10;
        number += cyfra;
        it++;
    }
    it++;

    for(int i = it; i < text.size(); i++){
        decimalcounter /= 10;
        temp = int(text[i]) - 48;
        temp *= decimalcounter;
        number += temp;
    }

    return number;
}

int string_to_int(std::string text){
    int number = 0;
    int cyfra = 0;

    for(int i = 0; i < text.size(); i++){
        cyfra = int(text[i]) - 48;
        number *= 10;
        number += cyfra;
    }

    return number;
}

void enb_position_reading(long double enbPositionArray[][dim]){
    std::fstream file;
    file.open("rrh_coordinates_10_4.txt", std::ios::in);
    std::string tmp;

    for(int i = 0; i < RRH; i++){
        for(int j = 0; j < dim; j++){
            file >> tmp;
            enbPositionArray[i][j] = string_to_double(tmp);
        }
    }

    file.close();
}

void ue_position_reading(long double uePositionArray[][dim]){
    std::fstream file;
    file.open("vehicle_coordinates_10_4.txt", std::ios::in);
    std::string tmp;

    for(int i = 0; i < vehicle; i++){
        for(int j = 0; j < dim; j++){
            file >> tmp;
            uePositionArray[i][j] = string_to_double(tmp);
        }
    }

    file.close();
}

void required_RBs_reading(int requiredRBs[][vehicle]){
    std::fstream file;
    file.open("required_rbs_10_4.txt", std::ios::in);
    std::string tmp;

    for(int i = 0; i < RRH; i++){
        for(int j = 0; j < vehicle; j++){
            file >> tmp;
            requiredRBs[i][j] = string_to_int(tmp);
        }
    }

    file.close();
}

int main (int argc, char *argv[])
{   
    long double simTime = 1.0; //time of simulation
    long double interPacketInterval = 1;
    int ENBS_NUMBER = 4; //constant number of eNBs
    int updatedENBS_NUMBER = 0; //constant number of eNBs that will be switched on
    int UES_NUMBER = 10; //constant number of UEs
    bool RRHstate[ENBS_NUMBER]; //array of flags to set 1 if RRH is on or 0 if RRH is off
    for(int i=0; i<ENBS_NUMBER; i++){
        RRHstate[i] = false;
    }
    int  vehToRRH[UES_NUMBER]; //array of attachment decision (which vehicle to which RRH)

    //array with enb positions (will be changed to be loaded from a file)
    long double enbPositionArray[RRH][dim];
    enb_position_reading(enbPositionArray);
    //array with ue positions (will be changed to be loaded from a file)
    long double uePositionArray[vehicle][dim];
    ue_position_reading(uePositionArray);

    //alghorithm for turning on RRHs and attaching UEs to RRHs
    int requiredRBs[RRH][vehicle];
    required_RBs_reading(requiredRBs);
    //in columns there are different vehicles and in rows are different RRhs 

    for(int i=0; i<UES_NUMBER; i++) //loop of making decision
    {
        int minRBreq = requiredRBs[i][0];
        int RRHid = 0;
        for(int k=0; k<ENBS_NUMBER; k++){
            if(requiredRBs[i][k] < minRBreq) {
                minRBreq = requiredRBs[i][k];
                RRHid = k;
            }
        }
        RRHstate[RRHid] = true;
        vehToRRH[i] = RRHid;
    }

    //create array with eNBs that will be switched on
    std::vector<long double> updatedEnbPositionArray[2];

    for(int i = 0; i < ENBS_NUMBER; i++){
        if(RRHstate[i] == true){
            updatedEnbPositionArray[0].push_back(enbPositionArray[i][0]);
            updatedEnbPositionArray[1].push_back(enbPositionArray[i][1]);
            updatedENBS_NUMBER++;
        }
    }
    NS_LOG_UNCOND("RRHs turned on");
    //LTE helper
    Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();
    lteHelper->SetAttribute ("PathlossModel", StringValue ("ns3::FriisSpectrumPropagationLossModel"));

    //EPC helper
    Ptr<PointToPointEpcHelper> epcHelper = CreateObject<PointToPointEpcHelper> (); 
    //lte helper has to know abuut epc helper
    lteHelper->SetEpcHelper (epcHelper);

    //create a remote host to provide simple service 
    Ptr<Node> pgw = epcHelper->GetPgwNode ();
    //create a single RemoteHost
    NodeContainer remoteHostContainer;
    remoteHostContainer.Create (1);
    Ptr<Node> remoteHost = remoteHostContainer.Get (0);
    //create internet stack helper
    InternetStackHelper internet;  
    internet.Install (remoteHostContainer);
    NS_LOG_UNCOND("RRHs turned on");
    //create the internet
    PointToPointHelper p2ph;
    p2ph.SetDeviceAttribute ("DataRate", DataRateValue (DataRate ("100Gb/s")));
    p2ph.SetDeviceAttribute ("Mtu", UintegerValue (1500));
    p2ph.SetChannelAttribute ("Delay", TimeValue (Seconds (0.010)));
    NetDeviceContainer internetDevices = p2ph.Install (pgw, remoteHost);
    Ipv4AddressHelper ipv4h;
    ipv4h.SetBase ("1.0.0.0", "255.0.0.0");
    Ipv4InterfaceContainer internetIpIfaces = ipv4h.Assign (internetDevices);
    //interface 0 is localhost, 1 is the p2p device
    Ipv4Address remoteHostAddr = internetIpIfaces.GetAddress (1);

    Ipv4StaticRoutingHelper ipv4RoutingHelper;
    Ptr<Ipv4StaticRouting> remoteHostStaticRouting = ipv4RoutingHelper.GetStaticRouting (remoteHost->GetObject<Ipv4> ());
    remoteHostStaticRouting->AddNetworkRouteTo (Ipv4Address ("7.0.0.0"), Ipv4Mask ("255.0.0.0"), 1);
    NS_LOG_UNCOND("Internet created");

    //nodes containers for eNBs and UEs
    NodeContainer enbNodes;
    enbNodes.Create (updatedENBS_NUMBER);
    NodeContainer ueNodes;
    ueNodes.Create (UES_NUMBER);

    //this is where positions of eNBs and UEs will be stored
    Ptr<ListPositionAllocator> enbpositionAlloc = CreateObject<ListPositionAllocator> ();
    Ptr<ListPositionAllocator> uepositionAlloc = CreateObject<ListPositionAllocator> ();

    //putting values of coordinates to simulation position array
    for(int i=0; i<updatedENBS_NUMBER; i++){
        enbpositionAlloc -> Add(Vector (updatedEnbPositionArray[0][i], updatedEnbPositionArray[1][i], 0));
    }

    NS_LOG_UNCOND(updatedENBS_NUMBER);
    MobilityHelper enbMobility;
    enbMobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
    enbMobility.SetPositionAllocator (enbpositionAlloc);
    enbMobility.Install (enbNodes);

    for(int i=0; i<UES_NUMBER; i++){
        uepositionAlloc -> Add(Vector (uePositionArray[i][0], uePositionArray[i][1], 0));
    }

    MobilityHelper ueMobility;
    ueMobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
    ueMobility.SetPositionAllocator (uepositionAlloc);
    ueMobility.Install (ueNodes);

    //installing devices
    NetDeviceContainer enbDevs;
    NetDeviceContainer ueDevs;
    enbDevs = lteHelper->InstallEnbDevice (enbNodes);
    ueDevs = lteHelper->InstallUeDevice (ueNodes);
    NS_LOG_UNCOND("UEs and eNBs prepared");
/*
    //assigning IPv4 addresses
    Ipv4InterfaceContainer ueIpInterface1;
    ueIpInterface1 = epcHelper->AssignUeIpv4Address(ueDevs);
*/


    //install the IP stack on the UEs

    internet.Install (ueNodes);
    Ipv4InterfaceContainer ueIpIface;
    ueIpIface = epcHelper->AssignUeIpv4Address (NetDeviceContainer (ueDevs));
    // Assign IP address to UEs, and install applications
    for (uint32_t u = 0; u < ueNodes.GetN (); ++u)
      {
        Ptr<Node> ueNode = ueNodes.Get (u);
        // Set the default gateway for the UE
        Ptr<Ipv4StaticRouting> ueStaticRouting = ipv4RoutingHelper.GetStaticRouting (ueNode->GetObject<Ipv4> ());
        ueStaticRouting->SetDefaultRoute (epcHelper->GetUeDefaultGatewayAddress (), 1);
      }
    //attach UEs to eNBs
    for(int i=0; i<UES_NUMBER; i++){
        lteHelper->Attach (ueDevs.Get (i), enbDevs.Get (vehToRRH[i]));  //we take a vehicle and search for the RRH it is supposed to be attached to
    }

  uint16_t dlPort = 1100;
  uint16_t ulPort = 2000;
  uint16_t otherPort = 3000;
  ApplicationContainer clientApps;
  ApplicationContainer serverApps;
    for (uint32_t u = 0; u < ueNodes.GetN (); ++u)
    {
        ++ulPort;
        //++otherPort;
        //PacketSinkHelper dlPacketSinkHelper ("ns3::UdpSocketFactory", InetSocketAddress (Ipv4Address::GetAny (), dlPort));
        PacketSinkHelper ulPacketSinkHelper ("ns3::UdpSocketFactory", InetSocketAddress (Ipv4Address::GetAny (), ulPort));
        PacketSinkHelper packetSinkHelper ("ns3::UdpSocketFactory", InetSocketAddress (Ipv4Address::GetAny (), otherPort));
        //serverApps.Add (dlPacketSinkHelper.Install (ueNodes.Get(u)));
        serverApps.Add (ulPacketSinkHelper.Install (remoteHost));
        //serverApps.Add (packetSinkHelper.Install (ueNodes.Get(u)));

        //UdpClientHelper dlClient (ueIpIface.GetAddress (u), dlPort);
        //dlClient.SetAttribute ("Interval", TimeValue (MilliSeconds(interPacketInterval)));
        //dlClient.SetAttribute ("MaxPackets", UintegerValue(1000000));

        UdpClientHelper ulClient (remoteHostAddr, ulPort);
        ulClient.SetAttribute ("Interval", TimeValue (MilliSeconds(interPacketInterval)));
        ulClient.SetAttribute ("MaxPackets", UintegerValue(1000000));

        //UdpClientHelper client (ueIpIface.GetAddress (u), otherPort);
        //client.SetAttribute ("Interval", TimeValue (MilliSeconds(interPacketInterval)));
        //client.SetAttribute ("MaxPackets", UintegerValue(1000000));

        //clientApps.Add (dlClient.Install (remoteHost));
        clientApps.Add (ulClient.Install (ueNodes.Get(u)));
        
        //if (u+1 < ueNodes.GetN ()){
        //    clientApps.Add (client.Install (ueNodes.Get(u+1)));
        //}
        
        //else{
          //  clientApps.Add (client.Install (ueNodes.Get(0)));
        //}
    }
    NS_LOG_UNCOND("Application installed");

    /*std::string experiment = "lte";
    std::string strategy = "testing";
    std::string input = "?";
    std::string runID;
    std::string description = "testardo";

    std::stringstream sstr;
    sstr << "run=" << time(NULL);
    runID = sstr.str();

    DataCollector data;
    data.DescribeRun(experiment, strategy, input, runID);
    data.AddMetadata("author", "rjjk");

    Ptr<CounterCalculator<> > appRx = CreateObject<CounterCalculator<> >();
    appRx->SetKey("receiver-rx-packets");
    receiver->SetCounter(appRx);
    data.AddDataCalculator(appRx);*/
/*
    Ipv4InterfaceContainer ueIpIface;
    Ipv4StaticRoutingHelper ipv4RoutingHelper;
    // assign IP address to UEs
    for (uint32_t u = 0; u < ueNodes.GetN (); ++u)
    {
        Ptr<Node> ue = ueNodes.Get (u);
        Ptr<NetDevice> ueLteDevice = ueDevs.Get (u);
        ueIpIface = epcHelper->AssignUeIpv4Address (NetDeviceContainer (ueLteDevice));
        // set the default gateway for the UE
        Ptr<Ipv4StaticRouting> ueStaticRouting;
        ueStaticRouting = ipv4RoutingHelper.GetStaticRouting (ue->GetObject<Ipv4> ());
        ueStaticRouting->SetDefaultRoute (epcHelper->GetUeDefaultGatewayAddress (), 1);
    }
*/
    //install apps on UE devices and on remote host
    /*uint16_t dlPort = 1234;
    PacketSinkHelper packetSinkHelper ("ns3::UdpSocketFactory",
                                        InetSocketAddress (Ipv4Address::GetAny (), dlPort));
    ApplicationContainer serverApps = packetSinkHelper.Install (ueNodes);
    serverApps.Start (Seconds (0.01));
    UdpClientHelper client (ueIpIface.GetAddress (0), dlPort);
    ApplicationContainer clientApps = client.Install (remoteHost);
    clientApps.Start (Seconds (0.01));
*/
    /*  
    // Activate a data radio bearer each UE (radio bearer automaticlly activated in attach method)
    enum EpsBearer::Qci q = EpsBearer::GBR_CONV_VOICE;
    EpsBearer bearer (q);
    lteHelper->ActivateDataRadioBearer (ueDevs, bearer);
    */

    Ptr<FlowMonitor> flowMonitor;
    FlowMonitorHelper flowHelper;
    flowMonitor = flowHelper.InstallAll();

    clientApps.Stop(Seconds (simTime));
    serverApps.Stop(Seconds (simTime));
    Simulator::Stop (Seconds (simTime + 2.0));

    Simulator::Run ();

    flowMonitor->SerializeToXmlFile("NameOfFile1s.xml", false, true);

    Simulator::Destroy ();
    return 0;
}
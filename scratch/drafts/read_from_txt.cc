#include "read_from_txt.hpp"
#include <iomanip>
#include <string>
#include <fstream>

int RRH = 4;
int vehicle = 10;

double string_to_double(string text){
    double number = 0;
    int it = 0;
    int cyfra = 0;
    double temp = 0;
    int decimalcounter = 1;

    while(text[it] != '.'){
        cyfra = int(text[it]) - 48;
        number *= 10;
        number += cyfra;
        it++;
    }
    
    it++;

    for(int i = it; i < text.size(); i++){
        decimalcounter /= 10;
        temp = int(text[i]) - 48;
        temp /= decimalcounter;+
        number += temp;
        it++;
    } 

    return number;
}

int string_to_int(string number){
    int number = 0;
    int cyfra = 0;

    for(int i = 0; i < text.size(); i++){
        cyfra = int(text[i]) - 48;
        number *= 10;
        number += cyfra;
    }
}

void enb_position_reading(double enbPositionArray[][2]){
    std::fstream file;
    file.open("../Results_txt/rrh_coordinates_10_4.txt", ios::in);
    string tmp;

    for(int i = 0; i < RRH; i++){
        file >> tmp;
        enbPositionArray[i][0] = string_to_int(tmp);

        file >> tmp;
        enbPositionArray[i][1] = string_to_int(tmp);
    }

    file.close();
}

void ue_position_reading(double uePositionArray[][2]){
    std::fstream file;
    file.open("../Results_txt/vehicle_coordinates_10_4.txt", ios::in);
    string tmp;

    for(int i = 0; i < vehicle; i++){
        file >> tmp;
        uePositionArray[i][0] = string_to_double(tmp);

        file >> tmp;
        uePositionArray[i][1] = string_to_double(tmp);
    }

    file.close();
}

void required_RBs_reading(double requiredRBs[][vehicle]){
    std::fstream file;
    file.open("../Results_txt/required_rbs_10_4.txt", ios::in);
    string tmp;

    for(int i = 0; i < RRH; i++){
        for(int j = 0; j < vehicle; j++){
            file >> tmp;
            uePositionArray[i][j] = string_to_int(tmp);
        }
    }

    file.close();
}

